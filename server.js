const express = require("express");
const dotenv = require("dotenv").config();
const asyncHandler = require("express-async-handler");
const connectDb = require("./connectDb");
const port = process.env.PORT || 5555;
const Product = require("./product")

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));
connectDb();

const getProducts = asyncHandler(
    async (req, res) => {
        const findByObj = {};

        Object.keys(req.query).forEach(key => {
            if (key !== "sortBy") {
                if (req.query[key]) findByObj[key] = req.query[key];
            }
        });

        const products = await Product.find(findByObj).sort(
            Array.isArray(req.query["sortBy"]) ? req.query["sortBy"]?.join(" ") : req.query["sortBy"]
        )
        res.status(200).json(products);
    }
);

const uploadProduct = asyncHandler(
    async (req, res) => {
        const product = await Product.create(req.body);
        res.status(200).json(product);
    }
);

const updateProduct = asyncHandler(
    async (req, res) => {
        const id = req.params.id;
        const product = await Product.findById(id);

        if (!product) {
            res.status(400);
            throw new Error("Nie znaleziono produktu.");
        }

        const updatedProduct = await Product.findByIdAndUpdate(
            id,
            req.body,
            {new: true}
        );

        res.status(200).json(updatedProduct);
    }
);

const deleteProduct = asyncHandler(
    async (req, res) => {
        const id = req.params.id;
        const product = await Product.findById(id);

        if (!product) {
            res.status(400);
            throw new Error("Nie znaleziono produktu.");
        }

        await Product.deleteOne({_id: id});

        res.status(200).json({id: id});
    });

const getReport = asyncHandler(
    async (req, res) => {
        const products = await Product.aggregate(
            [
                {
                    $project: {
                        _id: 0,
                        nazwa: 1,
                        ilosc: 1,
                        cena: 1,
                        localTotalPrice: {$multiply: ["$cena", "$ilosc"]},
                    }
                }
            ]
        );

        const totalPrice = Math.floor(
            products.map(it => it.localTotalPrice).reduce((acc, curr) => acc + curr, 0) * 100
        ) / 100;

        res.status(200).json({report: products, totalPrice: totalPrice});
    }
)

app.get("/products", getProducts);
app.post("/products", uploadProduct);
app.put("/products/:id", updateProduct);
app.delete("/products/:id", deleteProduct);
app.get("/products/report", getReport);

app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});