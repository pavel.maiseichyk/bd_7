const mongoose = require("mongoose");

const productSchema = mongoose.Schema(
    {
        nazwa: {
            unique: true,
            type: String,
            required: true,
        },
        cena: {
            type: Number,
            required: true,
        },
        opis: {
            type: String,
            default: "..."
        },
        ilosc: {
            type: Number,
            default: 0
        },
        jednostkaMiary: {
            type: String,
            enum: ["mm", "cm", "m"]
        }
    }
);

module.exports = mongoose.model('products', productSchema);